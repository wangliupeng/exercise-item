import java.awt.Graphics;
import java.applet.Applet;
public class HelloWorldApplet extends Applet {
	public String s;
	public void init() {
		s = new String("Hello World!");
	}
	public void paint(Graphics g) {
		super.paint(g);
		// 在浏览器中坐标为(25,25)的位置显示字符串s
		g.drawString(s, 25, 25); 
	}
}

