public class Rigorous {
	// Java中存在double和float精度不准的问题 
	public static void main(String[] args){
		double d = 10.00d;
		float f = 9.30f;
		double d1 = 9.30;
		System.out.println(d-f);
		System.out.println(d-d1);
		System.out.println(Double.MAX_VALUE);
		System.out.println(Math.pow(2,1024));
		System.out.println(Double.MIN_VALUE);
		System.out.println(Math.pow(2,-1074));
		System.out.println(0.05+0.01);  
        System.out.println(1.0-0.42);  
        System.out.println(4.015*100);  
        System.out.println(123.3/100);  
          
	}
}
