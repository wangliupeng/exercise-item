import javax.swing.JOptionPane;
public class DialogTest
{
	final static double PI = 3.1415926;  
	public static void main(String args[])
	{
		String  str = JOptionPane.showInputDialog(null,"请输入圆半径的值: \n","输入",JOptionPane.PLAIN_MESSAGE);
		try
       {
			double r = Double.parseDouble(str);
			double perimeter = 2*PI*r;
			double area = PI*r*r;
            JOptionPane.showMessageDialog(
                  null,
                  "半径为: "+r+"\n"+"圆周长为: "+perimeter+"\n"+"面积为: "+area+"\n",
				 "消息",
				 JOptionPane.QUESTION_MESSAGE);
       }
		catch( NumberFormatException e)
       {
           JOptionPane.showMessageDialog(
                  null,"输入为非法值",
                  "消息",
                  JOptionPane.PLAIN_MESSAGE);
       }
 	} 
}
