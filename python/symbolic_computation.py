'''
*************************************************************************
	> File Name: symbolic_computation.py
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: Thu 13 Apr 2017 05:51:43 PM CST
************************************************************************/
'''
from sympy import *

x, y, k = symbols('x,y,k')

u = (exp(0.5*sqrt(2)*(x+y))+exp(-0.5*sqrt(2)*(x+y)))\
    *(sin((y-x)/sqrt(2*k))+cos((y-x)/sqrt(2*k)))\
    -(x+y)*(x+y)/4
 
#u = (exp(x)+exp(-x))*(sin(y/sqrt(k))+cos(y/sqrt(k)))-x*x/2


ux = diff(u,x,1)
uy = diff(u,y,1)
uxx = diff(ux,x,1)
uxy = diff(ux,y,1)
uyx = diff(uy,x,1)
uyy = diff(uy,y,1)

#a11=1
#a22=k
#a21=a12=0

a11=a22=(1+k)/2
a12=a21=(1-k)/2

print(simplify(a11*uxx+a12*uxy+a21*uyx+a22*uyy))

