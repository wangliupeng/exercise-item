# read data from file 'data1'
# plot err to file 'l_infty_err.eps'

import numpy as np
import matplotlib.pyplot as plt

file_object = open('data1')

try:
	list_of_all_the_lines = file_object.read( ).splitlines( )
finally:
	file_object.close( )

#print(list_of_all_the_lines)

err = []
n = list_of_all_the_lines[0]
n = int(n)
for string in list_of_all_the_lines[1:n]:
	c = string.split('\t')
	uh = float(c[2])
	exactu = float(c[3])
	err.append(abs(uh-exactu))
	#uh.append(c2)

#print(exactu)

plt.figure(figsize=(8, 8), dpi=80)
plt.plot(err, color="black", linewidth=1.0, linestyle="-")
ax = plt.gca()
ax.set_ylim([0, max(err)])
strf = '%f' % max(err)
tit = '%s%s' % ('max=', strf)
plt.title(tit)
plt.xlabel('$i$')
plt.ylabel('$E_r$')
plt.savefig('l_infty_err.eps')
#plt.show()

