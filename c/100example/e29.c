/**
* @file  e29.c
* @brief 给一个不多于5位的正整数，
* 要求：一、求它是几位数，二、逆序打印出各位数字。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月11日 星期日 15时46分29秒
 */

#include<stdio.h>
#include<assert.h>

int main()
{
	long  n;
	printf("输入一个不多于5位的正整数:");
	scanf("%ld", &n);
	assert(n < 1000000 && n >0);

	printf("%ld",n);

	long quotient;       //商
	long remainder;      //余数
	long m = 0;
	int b = 0;
	do
	{
		b++;
		m *= 10;
		quotient  = n/10;
		remainder = n%10;

		m += remainder;
		n = quotient;
	}while(n);
	
	printf("为%d位数,逆序打印为%ld\n",b ,m);
	return 0;
}
