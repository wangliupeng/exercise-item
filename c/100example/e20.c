/**
* @file  e20.c
* @brief 一球从100米高度自由落下，每次落地后反跳回原高度的一半；
* 再落下，求它在第10次落地时，共经过多少米？第10次反弹多高？
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月22日 星期一 15时10分29秒
 */

#include<stdio.h>
int main()
{
	float h = 100, s;
	int n = 10;
	int i;
	s = h;
	for(i = 1; i < n; i++)
	{
		h /= 2;
		s += 2*h;
	}
	h /= 2;
	printf("第%d落地时共经过%f米\n", n, s);
	printf("第%d时反弹为%f米\n", n, h);
	
	return 0;
}
