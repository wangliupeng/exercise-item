/**
* @file  e27.c
* @brief 利用递归函数调用方式，将所输入的5个字符，以相反顺序打印出来。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月09日 星期五 10时13分57秒
 */

#include<stdio.h>

void print_char(int n);

int main()
{

	/*char ch[5];
	printf("请输入5个字符:");
	int i = 5;
	while(i)
	{
		scanf("%c", &ch[i-1]);
		i--;
	}

	for(i = 0; i < 5; i++)
	printf("%c", ch[i]);

	printf("\n");*/

	int i = 5;
	printf("输入5个字符:");
	print_char(i);
	printf("\n");
	return 0;
}

void print_char(int n)
{
	char next;
	if(n <= 1)
	{
		next = getchar();
		putchar(next);
	}
	else
	{
		next = getchar();
		print_char(n - 1);
		putchar(next);
	}
}


