/**
* @file  e17.c
* @brief 输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月20日 星期六 16时48分01秒
 */

/* 
 * getchar 由宏实现：#define getchar() getc(stdin）
 * getchar有一个int型的返回值.
 * 当程序调用getchar时.程序就等着用户按键.
 * 用户输入的字符被存放在键盘缓冲区中,直到用户按回车为止
 * (回车字符也放在缓冲区中)
 * 当用户键入回车之后，getchar才开始从stdio流中每次读入一个字符.
 * getchar函数的返回值是用户输入的字符的ASCII码，
 * 如出错返回-1，且将用户输入的字符回显到屏幕.
 */

#include<stdio.h>
int main()
{
	char c;
	int letter = 0;
	int blank = 0;
	int number = 0;
	int other = 0;
	printf("输入一行字符:");
	// 从键盘中读入字符
	while((c=getchar())!= '\n')
	{
		// 统计字母 = 不能少
		if((c >= 'A' && c <= 'Z')||(c >= 'a' && c <= 'z'))
			letter++;
		// 统计空格
		else if(c == ' ')
			blank++;
		// 统计数字，=不能少
		else if(c >= '0' && c <= '9')
			number++;
		else
			other++;
	}
	printf("该行字符含字母%d个,空格%d个，数字%d个，其他字符%d个\n",letter,blank,number,other);
	return 0;
}
