/*************************************************************************
	> File Name: e6.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年12月01日 星期一 13时36分34秒
 ************************************************************************/
/* 用*打印出LOVE*/

#include<stdio.h>

int main()
{
	printf("print LOVE with *:\n");
	printf(" *      *  *       *  ****\n ");
	printf("*     * *  *     *   *   \n ");
	printf("*    *   *  *   *    ****\n ");
	printf("*     * *    * *     *   \n ");
	printf("*****  *      *      ****\n ");
	return 0;
}
