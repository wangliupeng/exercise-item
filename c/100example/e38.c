/**
* @file  e38.c
* @brief 求一个3*3矩阵对角线元素之和  
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月21日 星期三 22时29分22秒
 */

#include<stdio.h>
#define N 3
int main()
{
	float matrix[N][N], sum = 0;
	int i,j;
	printf("请输入%dX%d的矩阵:\n", N, N);
	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
			scanf("%f", &matrix[i][j]);
	for(i = 0; i < N; i++)
		sum += matrix[i][i];
	printf("对角线元素之和:%f\n", sum);
	
	return 0;
}

