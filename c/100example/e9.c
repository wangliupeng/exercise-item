/*************************************************************************
	> File Name: e9.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年12月07日 星期日 16时34分41秒
 ************************************************************************/

#include<stdio.h>
/*输出国际象棋棋盘 8X8 黑白配*/
int main()
{
	int i,j;
	for(i = 0; i < 9; i++)
	{
		for(j = 0; j < 9; j++)
		{
			if((i+j)%2 == 0)
				printf("%c%c", 0xa8, 0x80); 
			//	printf("%c%c",219,219);  
			else
				printf("  ");
		}
		printf("\n");
	}

	return 0;
}

