/**
* @file  e13.c
* @brief 打印出所有的“水仙花数”，所谓“水仙花数”是指一个三位数，
* 其各位数字立方和等于该数本身。
* 例如：153是一个“水仙花数”，因为153=1的三次方＋5的三次方＋3的三次方
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月11日 星期四 10时55分50秒
 */

#include<stdio.h>
int main()
{
	printf("打印出所有的水仙花数：");
	int i,b,s,g,sum;
	for(i = 100; i < 1000; i++)
	{
		b = i/100;
		s = i/10%10;
		g = i%10;
		sum = b*b*b+s*s*s+g*g*g;
		if(sum == i)
		{
			printf("%d \t",sum);
		}
	}
	printf("\n");
	return 0;
}
