/*************************************************************************
	> File Name: e8.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年12月07日 星期日 16时14分58秒
 ************************************************************************/

#include<stdio.h>
/* 输出9*9口诀 */
int main()
{
	int i,j;
	for(i = 1; i <= 9; i++)
	{
		for(j = 1; j <= i; j++)
		{  
			printf("%dX%d=%d\t",j,i,i*j);
		}
		printf("\n");
	}
	return 0;
}
