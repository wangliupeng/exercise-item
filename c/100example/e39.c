/**
* @file  e39.c
* @brief 有一个已经排好序的数组。
* 现输入一个数，要求按原来的规律将它插入数组中。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月23日 星期五 14时55分30秒
 */

#include<stdio.h>
#define N 6
int main()
{
	int vec[N+1] = {0};
	printf("按从小到大的顺序输入%d个数到数组中:", N);
	int i;
	for(i = 0; i < N; i++)
		scanf("%d",&vec[i]);
	int num;
	printf("往数组中插入一个数:");
	scanf("%d", &num);
	//把num插入到数组中
	int j;
	for(i = 0; i < N; i++)
	{
		if( num < vec[i] )
		{		
			for(j = N; j > i; j--)
				vec[j] = vec[j-1];
			vec[i] = num;
			break;
		}
	}
	if(vec[N] == 0)
		vec[N] = num;
	printf("结果为:");
	for(i = 0; i < N+1; i++)
		printf("%d ", vec[i]);
	printf("\n");
	return 0;
}
