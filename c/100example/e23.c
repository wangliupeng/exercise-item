/**
* @file  e23.c
* @brief 打印出如下图案（菱形）
* * 
* * * * 
* * * * * * 
* * * * * * * * 
* * * * * * 
* * * * 
* * 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月25日 星期四 16时01分27秒
 */

#include<stdio.h>
int main()
{
	int m = 1, n = 0;
	int i;
	while(m >= 1)
	{
		for(i = 0; i < m; i++)
		{
			printf("* ");
			if(i == m - 1)
				n++;
		}
		printf("\n");
		if(n < 4) 
			m += 2;
		else
			m -= 2;
	}
	return 0;
}
