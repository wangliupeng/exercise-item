/**
* @file  e18.c
* @brief 求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个数字。
* 例如2+22+222+2222+22222(此时共有5个数相加)，几个数相加由键盘控制。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月21日 星期日 20时37分55秒
 */

#include<stdio.h>
int main()
{
	int a,n;
	printf("输入数字a和相加数的数目n:\n");
	scanf("%d %d", &a, &n);
	int i,j,la;
	int sum = 0;
	for(i = 0; i < n; i++)
	{
		la = a;
		j = i;
		while(j--)
		{
			la *= 10;
			la += a;
		}
		if(i == n-1)
			printf("%d",la);
		else
			printf("%d+",la);
		sum += la;
	}
	printf("=%d\n",sum);

	return 0;
}
