/**
* @file  e19.c
* @brief ：一个数如果恰好等于它的因子之和，这个数就称为“完数”。
* 例如6=1＋2＋3. 编程找出1000以内的所有完数。
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月22日 星期一 14时57分04秒
 */

/*
 * 什么是数的因子?因子就是所有可以整除这个数的数,不包括这个数自身.
 * 如15的因子是1，3，5 
 */

#include<stdio.h>
int main()
{
	int i, j, s;
	for(i = 2; i < 1000; i++)
	{
		s = 0;
		for(j = 1; j < i/2 + 1; j++)
		{
			if(i%j == 0)
				s += j;
		}
		if(i == s)
			printf("%d是完数\n",i);
	}
	return 0;
}
