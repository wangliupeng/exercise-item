/**
* @file  e25.c
* @brief 求1+2!+3!+...+20!的和 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月06日 星期二 11时39分35秒
 */

#include<stdio.h>
int main()
{
	int n = 20;
	int value = 1;
	int sum = 0;
	int i;
	for(i = 1; i <= n; i++)
	{
		value *= i;
		sum += value;
	}
	printf("1+2!+...+%d!=%d\n", n, sum);
	return 0;
}
