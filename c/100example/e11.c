/*************************************************************************
	> File Name: e11.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年12月09日 星期二 15时14分58秒
 ************************************************************************/

#include<stdio.h>
/* 有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月
 * 后每个月又生一对兔子，假如兔子都不死，问每个月的兔子总数为多少？*/
int main()
{
	int mouth;
	printf("输入月份：");
	scanf("%d",&mouth);
	int f,f1,f2;
	int i;
	for(i = 0; i < mouth; i++)
	{		
		if(i == 0)
		{
			f1 = 1;
			f = f1;
		}
		else if(i == 1)
		{	
			f2 = 1;
			f =f2;
		}
		else
		{
			f = f1 + f2;
			f1 = f2;
			f2 = f;
		}
	}
	printf("%d\n",f);
		
	return 0;
}
