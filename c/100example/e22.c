/**
* @file  e22.c
* @brief 两个乒乓球队进行比赛，各出三人。甲队为a,b,c三人，
* 乙队为x,y,z三人。以抽签决定比赛名单。有人向队员打听比赛的名单。
* a说他不和x比，c说他不和x,z比，请编程序找出三队赛手的名单。
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月24日 星期三 15时54分00秒
 */

#include<stdio.h>
int main()
{
	char a,b,c;
	for(a = 'y'; a <= 'z'; a++)
	{
			for(b = 'x'; b <= 'z'; b++)
			{			
				c = 'y';
				if(b != a && b != c && a != c)
					printf("a的对手为%c, b的对手为%c, c的对手为%c\n", 
							a, b, c);
			}
	}

	return 0;
}
