/**
* @file  e26.c
* @brief 利用递归方法求5!（阶乘：Factorial） 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月08日 星期四 16时55分27秒
 */

#include<stdio.h>
#include <assert.h>  // void assert( int exp );

int factorial(int n)
{
	assert(n > 0);
	int result;
	if(n == 1)
	{
		result = 1;
	}
	else
	{
		result = factorial(n - 1) * n;
	}
	return result;
}

int main()
{
	int n = 5;
	printf("%d!=%d\n", n, factorial(n));
	return 0;
}
