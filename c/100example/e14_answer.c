/**
* @file  e14_answer.c
* @brief 将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月15日 星期一 16时34分14秒
 */

#include<stdio.h>
int main()
{
	int n,i;
	printf("\nplease input a number:\n");
	scanf("%d",&n);
	printf("%d=",n);
	for(i=2;i<=n;i++)
	{
		while(n!=i)
		{
			if(n%i==0)
			{
				printf("%d*",i);
				n=n/i;
			}
			else
				break;
		}
	}
	printf("%d\n",n);
	return 0;
}
