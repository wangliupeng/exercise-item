/**
* @file  e36.c
* @brief 求100之内的素数 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月18日 星期日 15时39分48秒
 */

#include<stdio.h>
#define LOWER 2
#define UPPER 101
int main()
{
	unsigned int prime;

	printf("在%d和%d之间的素数有:\n", LOWER, UPPER);
	int i, flag;
	for(prime = LOWER; prime < UPPER; prime++)
	{
		flag = 0;

		for(i = LOWER; i < prime; i++)
			if(prime%i == 0)
			{
				flag = 1;
				break;
			}

		if(flag == 0)
			printf("%d ", prime);
	}
	printf("\n");
	return 0;
}

