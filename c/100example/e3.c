/*************************************************************************
	> File Name: e3.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年11月24日 星期一 15时52分20秒
 ************************************************************************/

/*
一个整数，它加上100后是一个完全平方数，再加上168又是一个完全平方数，请问该数是多少？
*/

/* 编译
 gcc e3.c -o e3 -std=c99 -lm
运行
./e3
  */

#include<stdio.h>
#include<math.h>

int main()
{
	/* 方法1*/
	int a = 0;
	for(int i = 0; i < 10000; i++)
		for(int k = 10; k < 100; k++)
		{
			if(i + 100 == k*k)
			{
				a = k;
				continue;
			}

			if(a != 0 && a*a + 168 == k*k)
			{
				printf("%d %d %d\n",i,a,k);
				a = 0;
			}

		}

	/* 方法2 */
	long int j,x,y;
	for(j=1;j<10000;j++)
	{
		x=sqrt(j+100);
		y=sqrt(j+268);
		if(x*x==j+100 && y*y==j+268)
			printf("%ld %ld %ld\n",j,x,y);
	}

	return 0;
}
