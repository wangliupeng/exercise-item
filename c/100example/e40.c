/**
* @file  e40.c
* @brief 将一个数组逆序并输出。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月24日 星期六 15时50分13秒
 */

#include<stdio.h>
#define N 8

int main()
{
	printf("输入%d个数字:", N);

	int vec[N];
	int i;
	for(i = 0; i < N; i++)
		scanf("%d", &vec[i]);
	
	int temp;
	for(i = 0; i < N/2; i++)
	{
		temp = vec[i];
		vec[i] = vec[N-i-1];
		vec[N-i-1] = temp;
	}


	for(i = 0; i < N; i++)
		printf("%d ", vec[i]);
	
	printf("\n");

	return 0;
}
