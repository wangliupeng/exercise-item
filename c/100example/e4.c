/*************************************************************************
	> File Name: e4.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年11月27日 星期四 10时07分38秒
 ************************************************************************/
/* 输入某年某月某日，判断这一天是这一年的第几天？*/
/* 4年一闰，100年不闰，400年一闰*/
/* 使用bool要带头文件stdbool.h*/
/* c语言中除c99外不能使用for(int i=0;;)...只能使用int i; for(i=0;;)...*/
/* error: stray ‘\357’ in program 表示多出了空格或使用了中文符号而不是英文符号*/
#include<stdio.h>
#include<stdbool.h>
int main()
{
	int year,month,day;
	printf("请输入？年？月？日:");
	scanf("%d %d %d",&year,&month,&day);
	bool flag = false;

	/*判断是闰年还是平年*/
	if((year%4==0&&year%100!=0)||year%400==0)
		flag = true;

	int mh[12]={31,28,31,30,31,30,31,31,30,31,30,31};
	
	/*闰年2月为29日，平年2月为28日*/
	if(flag == true)
		mh[1] = 29;
	
	int i,dy=0;
	for( i=0; i<month-1; i++)
		dy += mh[i];
	
	dy += day;
	
	printf("%d年%d月%d日为%d年第%d天\n",year,month,day,year,dy);

	return 0;
}
