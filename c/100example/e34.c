/**
* @file  e34.c
* @brief 练习函数调用 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月18日 星期日 15时33分49秒
 */

#include<stdio.h>
int count_apples();
int main()
{
	int number_of_apples;
	number_of_apples = count_apples();
	printf("%d", number_of_apples);
	return 0;
}

int count_apples()
{
	return 32;
}
	
