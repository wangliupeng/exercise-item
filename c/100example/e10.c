/*************************************************************************
	> File Name: e10.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年12月07日 星期日 16时51分17秒
 ************************************************************************/

#include<stdio.h>
/*打印ASCII码表 0-127 */
int main()
{
	int i;
	for(i = 0; i < 128; i++)
	{
		printf("%c ",i);
		if(i%15 == 0)
			printf("\n");
	}
	printf("\n");
	return 0;
}
