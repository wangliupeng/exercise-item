/**
* @file  e12.c
* @brief 判断101-200之间有多少个素数，并输出所有素数。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月10日 星期三 21时01分56秒
 */
/* if #inlcude<math.h>    gcc *.c –lm  */
#include<stdio.h>
#include<math.h>
int main()
{
	int i,j,d,s;
	for(i = 101; i < 201; i++)
	{
		s = 0;
		d = sqrt((double)i)+1;
		for(j = 2; j < d; j++)
		{
			if(i%j == 0)
			{
				s++;
				break;
			}
		}
		if(s == 0)
			printf("%d\t",i);			
	}
	printf("\n");
	return 0;
}


