/**
* @file  e15.c
* @brief 利用条件运算符的嵌套来完成此题：
* 学习成绩>=90分的同学用A表示，60-89分之间的用B表示,60分以下的用C表示。
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月16日 星期二 20时05分46秒
 */

#include<stdio.h>
int main()
{
	float score;
	printf("请输入分数：");
	scanf("%f", &score);
	char ch;

	ch =(score >= 90)?'A':((score >= 60)?'B':'C');
	
	printf("%f : %c\n", score, ch);

	return 0;
}
