/**
* @file  e28.c
* @brief 有5个人坐在一起，问第五个人多少岁？他说比第4个人大2岁。
* 问第4个人岁数，他说比第3个人大2岁。
* 问第三个人，又说比第2人大两岁。
* 问第2个人，说比第一个人大两岁。
* 最后问第一个人，他说是10岁。请问第五个人多大？
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月11日 星期日 15时34分25秒
 */

#include<stdio.h>
#include<assert.h>

int age(int person_number);

int main()
{
	printf("第5个人的岁数是%d\n", age(5));
	return 0;
}


int age(int person_number)
{
	assert(person_number > 0);
	if(person_number == 1)
		return 10;
	else
		return age(person_number - 1) + 2;
}


