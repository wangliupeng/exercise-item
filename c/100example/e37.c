/**
* @file  e37.c
* @brief 对10个数进行排序 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月20日 星期二 22时32分04秒
 */

#include<stdio.h>
#define N 10
void sort(int *vec, int n);

int main()
{
	int vec[N] = {5,2,13,9,21,3,6,1,4,29};
	int i;
	printf("排序前:");
	for(i = 0; i < N; i++)
        printf("%d ", vec[i]);
	printf("\n");
	sort(&vec, N);
	printf("排序后:");
	for(i = 0; i < N; i++)
        printf("%d ", vec[i]);
	printf("\n");
	return 0;
}

void sort(int *vec, int n)
{
	int min, temp;
        int i, j;
        for(i = 0; i < n-1; i++)
        {
		min = i;
                for(j = i+1; j < n; j++)
                {
                        if(vec[min] > vec[j]) 
                                min = j;
                }
                if(min != i)
                {
			temp = vec[i];
			vec[i] = vec[min];
			vec[min] = temp;
                }
        }
}

