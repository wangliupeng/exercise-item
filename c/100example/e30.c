/**
* @file  e30.c
* @brief 一个5位数，判断它是不是回文数。
* 即12321是回文数，个位与万位相同，十位与千位相同。
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月12日 星期一 18时34分32秒
 */

#include<stdio.h>

int main()
{
	unsigned long number;
	printf("输入一个5位数:");
	scanf("%ld", &number);
    if(number/10000 == 0)
	{
		printf("输入的数不是5位数\n");
	}
	else
	{
		if( (number%10) == (number/10000) && (number%100)/10 == (number/1000)%10)
			printf("%ld是回文数\n", number);
		else
			printf("%ld不是回文数\n", number);
	}

	return 0;
}
