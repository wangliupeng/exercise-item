/**
* @file  e14.c
* @brief 将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5。 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月15日 星期一 15时36分16秒
 */

#include<stdio.h>
#include<math.h>
int main()
{
	int i,flag=1;
	int m,sqr_m;

	printf("输入1个正整数:");
	scanf("%d", &m);

    	printf("%d=", m);

	while(flag)
	{	
		flag = 0;
		sqr_m = sqrt((double)m)+1;
		for(i = 2; i < sqr_m; i++)
		{
			if(m%i == 0)
			{
				flag = 1;
				printf("%d",i);
				m /= i;
				break;
			}
		}

		if(!flag)
		{
			printf("%d\n",m);
			continue;
		}
		
		printf("*");
	}


	return 0;
}
