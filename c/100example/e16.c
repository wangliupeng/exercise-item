/**
* @file  e16.c
* @brief 输入两个正整数m和n，求其最大公约数和最小公倍数 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月17日 星期三 16时33分41秒
 */

/* 1）最大公约数（最大公因数）就是几个数公有的因数中最大的一个。
 * 例12与18
 * 12的因数有1，12，2，6，3，4
 * 18的因数有1，18，2，9，6，3
 * 公有的因数有1，2，3，6，
 * 所以6就是12与18的最大公约数.
 *
 * 2）最小公倍数就是几个数公有的倍数中最小的一个。
 * 例4和6    
 * 4的倍数有4，8，12，16，20，24，……
 * 6的倍数有6，12，18，24，……
 * 4和6 公倍数 12，18……，
 * 所以4和6的最小公倍数是12 。
 */

/*辗转相除法
 * 两数为a、b(a>b)，求a和b最大公约数(a,b)的步骤如下：
 * 用b除a，得a÷b=q......r1(0≤r1）
 * 若r1=0，则（a,b)=b；
 * 若r1≠0，则再用r1除b，得b÷r1=q......r2 (0≤r2）
 *		  若r2=0，则（a，b)=r1，
 *        若r2≠0，则继续用r2除r1，……如此下去，直到能整除为止。
 *        其最后一个非零除数即为（a,b）
 * 
 * a、b的公倍数为 a*b/最大公约数
 * */

#include<stdio.h>
int main()
{
	int m, n;
	printf("请输入两个正整数:");
	scanf("%d %d",&m,&n);
	
	int a, b,temp;

        if(m < n)
	{
		a = n;
		b = m;
	}
	else
	{
		a = m;
		b = n;
	}

	while(b != 0) //辗转相除法
	{ 
		temp = a%b;
		a = b;
		b = temp;
	}

	printf("%d和%d的最大公约数为%d,最小公倍数为%d\n",m,n,a,m*n/a);

	return 0;
}
