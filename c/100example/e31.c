/**
* @file  e31.c
* @brief 请输入星期几的第一个字母来判断一下是星期几，
* 如果第一个字母一样，则继续判断第二个字母。
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月13日 星期二 22时44分01秒
 */

#include<stdio.h>
int main()
{
	char c1,c2;
	printf("请输入星期X的第一个字母(大写):");
	while((c1 = getchar()) != '\n')//在缓存中输入大写字母和换行符
	{
		switch(c1)
		{
			case 'M':
				printf("星期一\n");
				break;
			case 'T'://Tueday Thursday
				printf("请输入第二个字母(小写):");
				getchar(); //读入换行符
				c2 = getchar();//读写入的小写字母
				if(c2 == 'u')
					printf("星期二\n");
				else if(c2 == 'h')
					printf("星期四\n");
				else
					printf("未按要求输入字母\n");
				break;
			case 'W'://Wednesday
				printf("星期三\n");
				break;
			case 'F'://Friday
				printf("星期五\n");
				break;
			case 'S'://Saturday Sunday
				printf("请输入第二个字母(小写)：");
				getchar();
				c2 = getchar();
				if(c2 == 'a')
					printf("星期六\n");
				else if(c2 == 'u')
					printf("星期日\n");
				else
					printf("未按要求输入字母\n");
				break;
			default:
				printf("未按要求输入字母\n");
				break;
		}
	}
	return 0;
}
