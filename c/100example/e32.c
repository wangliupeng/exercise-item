/**
* @file  e32.c
* @brief 打印不同颜色的字体 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月14日 星期三 17时24分20秒
 */

#include<stdio.h>

#define NONE         "\033[m" 
#define GRAY         "\033[0;30m" //灰色
#define LIGHT_GRAY   "\033[1;30m"
#define RED          "\033[0;31m" //红色
#define LIGHT_RED    "\033[1;31m"
#define GREEN        "\033[0;32m" //绿色
#define LIGHT_GREEN  "\033[1;32m"
#define BROWN        "\033[0;33m" //棕色
#define YELLOW       "\033[1;33m" //黄色
#define BLUE         "\033[0;34m" //绿色
#define LIGHT_BLUE   "\033[1;34m"
#define PURPLE       "\033[0;35m" //紫色
#define LIGHT_PURPLE "\033[1;35m"
#define CYAN         "\033[0;36m" //青色
#define LIGHT_CYAN   "\033[1;36m"
#define WHITE        "\033[0;37m" //白色
#define LIGHT_WHITE  "\033[1;37m"

int main()
{

	printf(GRAY "gray " NONE);
	printf(LIGHT_GRAY "light_grey\n" NONE);
	printf(RED "red " NONE);
	printf(LIGHT_RED "light_red\n" NONE);
	printf(GREEN "green " NONE);
	printf(LIGHT_GREEN "light_green\n " NONE);
	printf(BROWN "brown " NONE);
	printf(YELLOW "yellow\n" NONE);
	printf(BLUE "blue " NONE);
	printf(LIGHT_BLUE "light_blue\n" NONE);
	printf(PURPLE "purple " NONE);
	printf(LIGHT_PURPLE "light_purple\n" NONE);
	printf(CYAN "cyan " NONE);
	printf(LIGHT_CYAN "light_cyan\n " NONE);
	printf(WHITE "white " NONE);
	printf(LIGHT_WHITE "light_white\n" NONE);
	return 0;
}
