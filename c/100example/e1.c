/*************************************************************************
	> File Name: e1.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年11月05日 星期三 19时58分59秒
 ************************************************************************/

/*
 * 由1、2、3、4这几个数字，能组成多少个互不相同且无重复数字的三位数？都是多少？
 */

#include<stdio.h>
int main()
{
	int num = 0;
	int i, j, k;
	for(i = 1; i < 5; i++)
		for(j = 1; j < 5 && j != i; j++)
			for(k = 1; k < 5; k++)
				if(i != j && j != k && k != i)
				{
					num++;
					printf("数%d：%d\n",num,100*i + 10*j + k);
				}
	return 0;
}
