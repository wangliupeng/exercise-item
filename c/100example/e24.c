/**
* @file  e24.c
* @brief 有一分数序列：2/1，3/2，5/3，8/5，13/8，21/13...
* 求出这个数列的前20项之和 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月05日 星期一 16时52分16秒
 */

#include<stdio.h>

int main()
{
	float numerator = 2.0;
	float denominator = 1.0;
	float tmp, sum = 0;
	int i, n = 20;
	for(i = 0; i < n; i++)
	{
		sum += numerator/denominator;
		tmp = denominator;
		denominator = numerator;
		numerator += tmp;
	}
	printf("分数序列2/1,3/2...,前%d之和为%f\n", n, sum);
	return 0;
}
