#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "mpi.h"


#define n 20 //!x,y方向的网格数
#define np 2 //! 进程数
#define PI 3.1415926535897932
#define EPS 1e-6

double uu(double,double);
double ff(double,double);


int main(int argc,char **argv)
{
	int i,j,count;						//! 计数器
	int jn=n/np;						//! 各进程局部y方向网格数
	double err,l2err,l2err0;			//! 误差
	double res,maxres,maxres0;          //! 残差
	double xx,yy,h,hh;					//! 坐标及步长
	double Uh[jn+2][n+1];				//! 各进程对应局部网格矩阵,存放近似解
	double Ur[jn+2][n+1];				//! 各进程对应局部网格矩阵,存放精确解
	double Uu[jn+1][n+1];				//! 迭代辅助变量
	double F[jn+1][n+1];				//! 右端项f在网格格点上的值
	int nprocs,myrank,myupper,mylower;  //! 进程相关量
	int jstart,jend;					//! 内部网格y方向结点起始、终止坐标
	int htype;							//! MPI自定义数据类型(y方向)
	double tstart,tend;
	MPI_Status status;

	//! 初始化MPI
	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
	//printf("n=%d np=%d nprocs=%d\n",n,np,nprocs);
	if(n%nprocs!=0||nprocs!=np)
	{							
		printf("y方向网格数必须是进程数倍数!\n");
		MPI_Finalize();
		return 1;
	}

	//! 确定各进程及相邻进程的进程号
	MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
	myupper=myrank+1;
	if (myupper>=nprocs) myupper=MPI_PROC_NULL;
	mylower=myrank-1;
	if (mylower<0) mylower=MPI_PROC_NULL;

	//! 基本变量赋值
	h=1.0/n;
	hh=h*h;
	jstart=1;jend=jn;
	if (myrank==np-1) jend-=1;

	//! 数据类型定义
	MPI_Type_contiguous(n-1,MPI_DOUBLE,&htype);
	MPI_Type_commit(&htype);			//! x方向,与上下进程交换数据

	//! 初始化迭代矩阵
	for (j=jstart-1;j<jend+2;j++)
	{
		for (i=0;i<n+1;i++)
		{
			xx=i*h;
			yy=(j+myrank*jn)*h;
			if (j>=jstart&&j<=jend&&i>=1&&i<=n-1)
			{
				Uh[j][i]=0.0;				//! 近似解赋初值
				Ur[j][i]=uu(xx,yy);			//! 精确解
				F[j][i]=hh*ff(xx,yy);		//! 右端项
			}
			else
			{
				if ((i==0)||(j==(jstart-1)&&myrank==0)||
					(i==n)||(j==(jend+1)&&myrank==(np-1)))
					{
						Uh[j][i]=uu(xx,yy);	//! 给定边界值
					}
			}
		
		
		}
	}

	//! Jacobi迭代求解
	count=0;
	tstart=MPI_Wtime();
	do					
	{
		count++;
		//! 发送下、上边界
		MPI_Send(*(Uh+1)+1,1,htype,mylower,count+100,MPI_COMM_WORLD);
		MPI_Send(*(Uh+jend)+1,1,htype,myupper,count+100,MPI_COMM_WORLD);
		//! 接收下、上边界
		MPI_Recv(*(Uh)+1,1,htype,mylower,count+100,MPI_COMM_WORLD,&status);
		MPI_Recv(*(Uh+jend+1)+1,1,htype,myupper,count+100,MPI_COMM_WORLD,&status);

		for (j=jstart;j<=jend;j++)
		{
			for (i=1;i<=n-1;i++)
			{
				Uu[j][i]=F[j][i]+(Uh[j-1][i]+Uh[j+1][i]+Uh[j][i-1]+Uh[j][i+1]);
				Uu[j][i]/=4;
                Uh[j][i]=Uu[j][i];
			}
		}

		//! 发送上下边界
		MPI_Send(*(Uh+1)+1,1,htype,mylower,count+110,MPI_COMM_WORLD);
		MPI_Send(*(Uh+jend)+1,1,htype,myupper,count+110,MPI_COMM_WORLD);
		//! 接收上下边界
		MPI_Recv(*(Uh)+1,1,htype,mylower,count+110,MPI_COMM_WORLD,&status);
		MPI_Recv(*(Uh+jend+1)+1,1,htype,myupper,count+110,MPI_COMM_WORLD,&status);

		//! 计算误差
		maxres = 0.;
		for (j=jstart;j<=jend;j++)
		{
			for (i=1;i<=n-1;i++)
			{
				res = F[j][i]+(Uh[j-1][i]+Uh[j+1][i]+Uh[j][i-1]+Uh[j][i+1])-4*Uh[j][i];
				maxres = (maxres<res)?res:maxres;
			}	
		}
		MPI_Allreduce(&maxres,&maxres0,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
		err = maxres0;	

		//if (myrank==0&&count%50==0)
			//printf("count=%d,derr=%.15le\n",count,derr);
		
	} while (err>=EPS);//! 收敛性判断
	
	//! 计算l2离散误差
	l2err = 0.;
	for (j=jstart;j<=jend;j++)
	{
		for (i=1;i<=n-1;i++)
		{
			l2err+=(Uh[j][i]-Ur[j][i])*(Uh[j][i]-Ur[j][i]);
		}
	}
	MPI_Allreduce(&l2err,&l2err0,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
	l2err=sqrt(l2err0/((n+1)*(n+1)));

	tend=MPI_Wtime();
	if (myrank==0)
	{
		printf("本次计算网格为 %d X %d, 数值解与真解的l2离散误差为：%10.8e\n",n,n,l2err);
		printf("雅克比（jacobi）迭代次数为：%d\n",count);
		printf("本次执行使用进程个数为：%d，整个算法消耗的时间为：%lf s \n",nprocs,tend-tstart);	
	}

	//! 结束MPI调用
	MPI_Finalize();
}

//! 精确解
inline double uu(double x,double y)
{
	return (sin(PI*x)*sin(PI*y));					
}

//! 右端函数
inline double ff(double x,double y)
{
	return (2.0*PI*PI*uu(x,y));						
}

