#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<malloc.h>
#include<omp.h>

#define n 20    //x,y方向的网格数
#define PI 3.1415926535897932
#define EPS 1e-6

double uu(double,double);
double ff(double,double);

int main(int argc, char** argv)
{
	//! 从命令行获取进程总数
	int nprocs = strtol(argv[1],NULL,10);
	if (n%nprocs!=0)
	{							
		printf("y方向网格数必须是进程数倍数\n");
		return 1;
	}

	//! 定义矩阵
	double Uh[n+1][n+1];				// 近似解
	double Ur[n+1][n+1];				// 精确解
	double Uu[n+1][n+1];				// 辅助变量
	double F[n+1][n+1];					// 右端项f(x,y)在网格格点上的值

	//! 定义变量
	int i,j;
	double xx,yy;
	double h=1.0/n;
	double hh=h*h;
	
	//! 初始化
	#pragma omp parallel for num_threads(n) \
		default(none) private(i,j,xx,yy) shared(Uh,Uu,Ur,F,h,hh)
	for (j=0;j<n+1;j++)
	{
		for (i=0;i<n+1;i++)
		{
			xx=i*h;
			yy=j*h;
			if (j>=1&&j<=n-1&&i>=1&&i<=n-1)
			{
				Uh[j][i]=0.0;				// 近似解赋初值
				Uu[j][i]=0.0;				// 辅助近似解赋初值
				Ur[j][i]=uu(xx,yy);			// 精确解
				F[j][i]=hh*ff(xx,yy);		// 右端项
				//printf("Uh[%d][%d]=%lf\n",j,i,Uh[j][i]);
			}
			else
			{
				Uh[j][i]=uu(xx,yy);	// 给定边界值
				Uu[j][i]=uu(xx,yy);	
			}
		}
	}

	int count=0;
	double err,l2err,res,maxres;
	double tstart=omp_get_wtime();
	do
	{
		count++;
		#pragma omp parallel for num_threads(nprocs) \
		default(none) private(i,j) shared(Uh,Uu,F) 
		for (j=1;j<=n-1;j++)
		{
			for (i=1; i<=n-1; i++)
			{
				Uu[j][i]=F[j][i]+(Uh[j-1][i]+Uh[j+1][i]+Uh[j][i-1]+Uh[j][i+1]);
				Uu[j][i]/=4;
				//printf("%d, Uu[%d][%d]=%lf\n",myrank,j,i,Uu[j][i]);
			}
		}
		
		maxres = 0.;
		#pragma omp parallel for num_threads(nprocs) \
		default(none) private(i,j,res) shared(F,Uh,Uu) reduction(max:maxres) 
		for (j=1;j<=n-1;j++)
		{
			for (i=1; i<=n-1; i++)
			{
				Uh[j][i]=Uu[j][i];
				res = F[j][i]+(Uu[j-1][i]+Uu[j+1][i]+Uu[j][i-1]+Uu[j][i+1])-4*Uu[j][i];
				maxres = (maxres<res)?res:maxres;
			}	
		}
		err = maxres;
		//printf("count=%d,serr=%.15le\n",count,serr);
	} while(err>=EPS); // 收敛性判断

	
	//! 计算l2离散误差
	l2err = 0.;
	#pragma omp parallel for num_threads(nprocs) \
		default(none) private(i,j) shared(Uh,Ur) reduction(+:l2err)
	for (j=1;j<=n-1;j++)
	{
		for (i=1; i<=n-1; i++)
		{
			l2err+=(Uh[j][i]-Ur[j][i])*(Uh[j][i]-Ur[j][i]);
		}
	}
	l2err=sqrt(l2err/((n+1)*(n+1)));	

	double tend=omp_get_wtime();

    printf("本次计算网格为 %d X %d, 数值解与真解的l2离散误差为：%10.8e\n",n,n,l2err);
	printf("雅克比（jacobi）迭代次数为：%d\n",count);
	printf("本次执行使用进程个数为：%d，整个算法消耗的时间为：%lf s \n",nprocs,tend-tstart);	
	
	return 0;
}

//! 精确解
inline double uu(double x,double y)
{
	return (sin(PI*x)*sin(PI*y));					
}

//! 右端函数
inline double ff(double x,double y)
{
	return (2.0*PI*PI*uu(x,y));						
}


