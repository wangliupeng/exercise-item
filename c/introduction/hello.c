/**
* @file  hello.c
* @brief print "hello, world!" 
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2015年01月27日 星期二 16时59分09秒
 */

#include<stdio.h>                       
int main()
{
	printf("hello, world");
	return 0;
}
