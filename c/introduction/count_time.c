/**
* @file  time.c
* @brief c计算程序运行时间
* @author: wangliupeng
* wangliupeng@xtu.edu.cn 
* @data 2014年12月10日 星期三 21时34分43秒
 */

#include<stdio.h> 
#include <stdlib.h>  
#include <time.h> 
  
int main()  
{  
	   long    i = 10000000L;  
	   clock_t start, finish;  
	   double  duration;  
	   /* 测量一个事件持续的时间*/ 
	   printf( "Time to do %ld empty loops is ", i );  
	   start = clock();  
	   while( i-- );  
	   finish = clock();  
	   duration = (double)(finish - start) / CLOCKS_PER_SEC;  
	   printf( "%f seconds\n", duration );    
	   return 0;
} 
