/*************************************************************************
	> File Name: main.c
	> Author: wangliupeng
	> Mail: wangliupeng@xtu.edu.cn 
	> Created Time: 2014年10月22日 星期三 19时23分42秒
 ************************************************************************/

#include<stdio.h>

/** 百鸡问题
 * 鸡翁一值钱五，鸡母一值钱三，鸡雏三值钱一。
 * 百钱买百鸡，问鸡翁、鸡母、鸡雏各几何？
 * cocks 鸡翁； hens 鸡母; chicks 鸡雏
 */
int main()
{
	int cocks,hens,chicks;
	cocks=0;
	while(cocks<=19)
	 {
		 hens=0;
		 while(hens<=33)
		 {
		   chicks=100-cocks-hens;
		   if(5*cocks+3*hens+chicks/3==100 && chicks%3==0)
			   printf("%d,%d,%d\n",cocks,hens,chicks);
		    hens=hens+1;
		 }
		 cocks=cocks+1;
	 }
  return 0;
}
