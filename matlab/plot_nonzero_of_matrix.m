A=[1,0,0,1;0,1,0,1;0,0,1,1;1,1,0,0];

row = size(A,1);
col = size(A,2);

for i = 1:row
    for j = 1:col
        if A(i,j) ~= 0
            plot(i,j,'k.');
            hold on
        end
    end
end
xlim([0,row+1]);
ylim([0,col+1]);
axis square;
grid on;
set(gca,'xtick',1:row)
set(gca,'ytick',1:col)

    